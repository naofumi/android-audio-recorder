# How To Translate

To translate 'Audio Recorder' to your language you need to translate following files:

  * [pref_general.xml](/app/src/main/res/xml/pref_general.xml)
  * [strings.xml](/app/src/main/res/values/strings.xml)

Additional file from 'android-library'
  * [https://gitlab.com/axet/android-library/.../strings.xml](https://gitlab.com/axet/android-library/blob/master/src/main/res/values/strings.xml)

Additional file from 'android-audio-library'
  * [https://gitlab.com/axet/android-audio-library/.../strings.xml](https://gitlab.com/axet/android-audio-library/blob/master/src/main/res/values/strings.xml)

Also, add Google Play translation for:
  * Title (50 symbols max)
  * Short Description (80 symbols max)
  * Full description (4000 symbols max)

Then add those files to the repository using "New Issue" or "Merge Request" (GitLab's name for "Pull Request" if you come from GitHub) against the `dev` branch.
